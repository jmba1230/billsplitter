let split ={
	bill: 0,
	share: 0,
	total: 0
}


let mainPage = document.getElementById('main-page');


const submitBtn = document.getElementById('submit-btn');
submitBtn.addEventListener('click', function(){


	let bill =mainPage.lastElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.value;

	let share = mainPage.lastElementChild.firstElementChild.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.firstElementChild.value;

	split.bill = parseFloat(bill);
	split.share =parseFloat(share);

	split.total = split.bill/split.share;

	// display bill/share=total
	mainPage.lastElementChild.lastElementChild.lastElementChild.textContent = split.total;
		
});
